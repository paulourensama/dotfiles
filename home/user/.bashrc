# bashrc file

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Prompt color
if ["$color_prompt" = yes ]; then
	PS1="\[\033[0;30m\]\u@\h\[\033[0;30m\] \W\[\033[0;30m\] \$\[\033[0m\] "

# alias
alias ls='ls --color=auto'
